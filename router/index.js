import express from 'express'; 
import json from 'body-parser'; 

export const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', { titulo: "Compañia de Luz", nombre: "Brandon Pastrano" });
});
router.get('/factura', (req, res) => {
    const params = {
    };
    res.render('factura', params);
});
router.post('/factura', (req, res) => {
    // Obtener los datos del cuerpo de la solicitud
    let { numero_recibo, nombre, domicilio, tipo_servicio, kilowatts_consumidos } = req.body;
    let costoPorKilowatt = 0;
    if(tipo_servicio=="1"){
        costoPorKilowatt=1.08;
    }   
    else if(tipo_servicio=="2"){
        costoPorKilowatt=2.5;
    }
    else{
        costoPorKilowatt=3.0;
    } 
    

    // Calcular el subtotal
    const subtotal = costoPorKilowatt * kilowatts_consumidos;

    // Calcular el impuesto
    const impuestos = subtotal * 0.16;

    // Calcular el descuento
    let descuentos = 0;
    if (kilowatts_consumidos <= 1000) {
        descuentos = subtotal * 0.10;
    } else if (kilowatts_consumidos <= 10000) {
        descuentos = subtotal * 0.20;
    } else {
        descuentos = subtotal * 0.50;
    }

    // Calcular el total a pagar
    const totalPagar = (subtotal + impuestos) - descuentos;

    // Renderizar la página con los datos calculados para ambas tablas
    res.render('factura', {
        nombre,
        tipo_servicio,
        kilowatts_consumidos,
        costoPorKilowatt,
        tipo_servicio,
        subtotal,
        impuestos,
        descuentos,
        totalPagar
    });
});

export default { router };