//const express = require("express");
//const http = require("http");

import express from 'express';
import http from 'http';
import { fileURLToPath } from 'url';
import json from 'body-parser';
import path from 'path'; 
import { router as misRutas } from './router/index.js';

const puerto = 80;

const _filename = fileURLToPath(import.meta.url);
const _dirname = path.dirname(_filename);
//declarar la variable punto de inicio

const main= express();

main.set("view engine","ejs");
main.use(express.static(_dirname + '/public'));
main.use(json.urlencoded({extended:true}));
main.use(misRutas);
//declarar primer ruta por omisión

main.listen(puerto,()=>{
    console.log("Se inicio el servidor por el puerto: " + puerto);
})